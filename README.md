# fees.wtf

Calculate fees for multichain network.

<a href="https://fees.sigri44.com" target="_blank">fees.sigri44.com</a>

## Supported chains
- Ethereum
- Polygon
- Binance Smart Chain
- xDai
- Fantom

## Coming Soon
- Avalanche (waiting for API access)
